pipeline {
    agent {
        label 'Pier12'
    }

    stages {

        stage('Clone') {
            steps {
                git credentialsId: 'ci-Bitbucket SSH Key', url: 'https://bitbucket.org/ibensalem/ci-artifactory.git'
            }
        }

        stage('clean') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins-upload', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh "./gradlew clean -PartifactoryUser=$USERNAME -PartifactoryPassword=$PASSWORD"
                }
            }
        }

        stage('Compile') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins-upload', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh "./gradlew build -x test -PartifactoryUser=$USERNAME -PartifactoryPassword=$PASSWORD"
                }
            }
        }

        stage('Test') {
            steps{
                withCredentials([usernamePassword(credentialsId: 'jenkins-upload', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh './gradlew test jacocoTestReport -PartifactoryUser=$USERNAME -PartifactoryPassword=$PASSWORD'
                }
            }
        }

        stage('Sonar') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins-upload', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    withCredentials([usernamePassword(credentialsId: 'sonar-ci', usernameVariable: 'SONARLOGIN', passwordVariable: 'SONARKEY')]) {
                        sh "./gradlew sonarqube -PartifactoryUser=$USERNAME -PartifactoryPassword=$PASSWORD -Psonar.login=$SONARKEY"
                    }
                }
            }
        }

        stage('Reports') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins-upload', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh "./gradlew javadoc asciidoctor site -PartifactoryUser=$USERNAME -PartifactoryPassword=$PASSWORD"
                }
            }
        }
    }

    post {
        always {
            junit allowEmptyResults: true, testResults: 'build/test-results/TEST-*.xml'
            publishHTML(target : [
                allowMissing: false,
                alwaysLinkToLastBuild: true,
                keepAll: true,
                reportDir: 'build/site',
                reportFiles: 'index.html',
                reportName: 'Project Site',
                reportTitles: 'The Project Site']
            )
        }
    }
} 

    

