package ext.gemalto.pdm.generic.guiaction.createlinkparts.processors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;


import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ptc.core.components.forms.DefaultObjectFormProcessor;
import com.ptc.core.components.forms.FormResult;
import com.ptc.netmarkets.util.beans.NmCommandBean;

import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.PersistenceServerHelper;
import wt.fc.QueryResult;
import wt.fc.ReferenceFactory;
import wt.inf.container.WTContainer;
import wt.inf.container.WTContainerHelper;
import wt.inf.container.WTContainerRef;
import wt.log4j.LogR;
import wt.part.WTPart;
import wt.part.WTPartMaster;
import wt.part.WTPartUsageLink;
import wt.query.ClassAttribute;
import wt.query.ConstantExpression;
import wt.query.QuerySpec;
import wt.query.RelationalExpression;
import wt.query.SearchCondition;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;

public class CreateLinkParentChildFormProcessor extends DefaultObjectFormProcessor {
    
    private static final String CLASSNAME = CreateLinkParentChildFormProcessor.class.getName();
    private static final Logger LOGGER = LogR.getLogger(CLASSNAME);

    @Override
    public FormResult doOperation(NmCommandBean bean, List list) {

        FormResult result = new FormResult();

        try {

            HashMap map = bean.getText();

            Object[] keys = map.keySet().toArray();

            String numberParent = "";
            String numberChild = "";
            String numberDummy = "";
            
            for (Object key : keys) {

                LOGGER.debug("key: " + key);
                if (StringUtils.contains((String) key, "numberParent")) {
                    numberParent = (String) map.get(key);
                } else if (StringUtils.contains((String) key, "numberChild")) {
                    numberChild = (String) map.get(key);
                } else if (StringUtils.contains((String) key, "numberDummy")) {
                    numberDummy = (String) map.get(key);
                }
            }

            LOGGER.debug("numberParent: " + numberParent + " numberChild: " + numberChild);
            createUsageLink(numberParent, numberChild);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }

    public static void createUsageLink(String numberParent, String numberChild) throws Exception {

        WTPart parent = (WTPart) findRevisionFromNumberAndVersion(numberParent).get(0);
        WTPart child = (WTPart) findRevisionFromNumberAndVersion(numberChild).get(0);

        if (parent != null && child != null) {
            // create link between parent object and the selected chosen child
            WTPartUsageLink newUsageLink = WTPartUsageLink.newWTPartUsageLink(parent, (WTPartMaster) child.getMaster());
            PersistenceServerHelper.manager.insert(newUsageLink);
        } else {
            LOGGER.debug("parent or child is null:");
        }
    }

    public static Vector<Persistable> findRevisionFromNumberAndVersion(String number) throws WTException, ClassNotFoundException {

        Class searchInstance = Class.forName(WTPart.class.getName());
        QuerySpec queryspec = new QuerySpec();
        int currentClassIndex = queryspec.appendClassList(searchInstance, true);
        ClassAttribute classattribute = new ClassAttribute(searchInstance, "master>number");
        // number param
        RelationalExpression expression = ConstantExpression.newExpression(number, classattribute.getColumnDescriptor().getJavaType());
        SearchCondition condition = new SearchCondition(classattribute, SearchCondition.EQUAL, expression);
        queryspec.appendWhere(condition, new int[] { currentClassIndex });
        queryspec.appendAnd();

        classattribute = new ClassAttribute(searchInstance, "iterationInfo.latest");
        // lastest iteration param
        expression = ConstantExpression.newExpression("1", classattribute.getColumnDescriptor().getJavaType());
        condition = new SearchCondition(classattribute, SearchCondition.EQUAL, expression);
        
        queryspec.appendWhere(condition, new int[] { currentClassIndex });        
        LOGGER.debug("queryspec: " + queryspec.toString());        
        
        QueryResult qr = PersistenceServerHelper.manager.query(queryspec);
        LOGGER.debug("qr size: " + qr.size());
               
        Vector<Persistable> vectorVersions = new Vector<Persistable>();

        while (qr.hasMoreElements()) {
            Persistable p = ((Persistable[]) qr.nextElement())[0];
            vectorVersions.add(p);
        }

        LOGGER.debug("vectorVersions size: " + vectorVersions.size());
        return vectorVersions;
    }

}