package ext.gemalto.pdm.generic.guiaction.createlinkparts.rb;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("ext.gemalto.pdm.generic.guiaction.createlinkparts.rb.ActionsResourceRB")
public final class ActionsResourceRB extends WTListResourceBundle {

    @RBEntry("createLinkParentChild")
    public static final String CREATEPARENTCHILDLINK_DESCRIPTION = "part.createLinkParentChild.description";
    @RBEntry("createLinkParentChild")
    public static final String CREATEPARENTCHILDLINK_TOOLTIP = "part.createLinkParentChild.tooltip";
    @RBEntry("createLinkParentChild Step")
    public static final String CREATEPARENTCHILDLINKSTEP1_DESCRIPTION = "part.createLinkParentChild.description";
    @RBEntry("createLinkParentChild Step")
    public static final String CREATEPARENTCHILDLINKSTEP1_TOOLTIP = "part.createLinkParentChild.tooltip";

}