<%@taglib prefix="jca"
	uri="http://www.ptc.com/windchill/taglib/components"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/mvc" prefix="mvc"%>
<%@include file="/netmarkets/jsp/components/beginWizard.jspf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jca:wizard title="Create Link Parent Child">
	<jca:wizardStep action="createLinkParentChildStep1" type="part" />
</jca:wizard>

<%@ include file="/netmarkets/jsp/util/end.jspf"%>