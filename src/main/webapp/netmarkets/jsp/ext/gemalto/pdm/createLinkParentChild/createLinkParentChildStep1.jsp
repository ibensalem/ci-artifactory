<%@ taglib prefix="wctags" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/mvc" prefix="mvc"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/components" prefix="jca"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/wrappers" prefix="gui"%>

<%@include file="/netmarkets/jsp/components/beginWizard.jspf"%>
<%@include file="/netmarkets/jsp/components/includeWizBean.jspf"%>


<table>
	<tr>
	    <td scope="row" width="50" class="tableColumnHeaderfont" align="right">Number Parent: </td>
	    <td scope="row" width="50" class="tabledatafont" align="left">&nbsp;
	        <gui:textBox name="numberParent" id="numberParent" />
	     </td>
	</tr>
	
	<tr>
	    <td scope="row" width="50" class="tableColumnHeaderfont" align="right">Number Child: </td>
	    <td scope="row" width="50" class="tabledatafont" align="left">&nbsp;
	        <gui:textBox name="numberChild" id="numberChild" />
	     </td>
	</tr>
</table>

<%@ include file="/netmarkets/jsp/util/end.jspf"%>



