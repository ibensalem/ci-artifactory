package ext.gemalto.pdm.generic.guiaction.createlinkparts;



import ext.gemalto.pdm.generic.guiaction.createlinkparts.processors.CreateLinkParentChildFormProcessor;
import org.junit.jupiter.api.Test;
import wt.httpgw.GatewayAuthenticator;
import wt.method.RemoteMethodServer;

public class CreatePartUnitTest {
    
    private void testExecution(String user, String objectNumberP, String objectNumberC) throws Exception {

        RemoteMethodServer rms = RemoteMethodServer.getDefault();
        GatewayAuthenticator auth = new GatewayAuthenticator();
        auth.setRemoteUser(user);
        rms.setAuthenticator(auth);

        CreateLinkParentChildFormProcessor.createUsageLink(objectNumberP, objectNumberC);

    }

    @Test
    public void CheckOutAndInUnitTestOrderItem1() throws Exception {
        testExecution("wcadmin", "O1049559", "O1049558");
    }

}
